// Fill out your copyright notice in the Description page of Project Settings.


#include "StateEffects/TDSStateEffect.h"
#include "Character/TDSHealthComponent.h"
#include "Interface/TDSIGameActor.h"
#include "Kismet/GameplayStatics.h"
#include "GameFramework/PlayerController.h"
#include "Net/UnrealNetwork.h"
#include "Character/TDSCharacter.h"

bool UTDSStateEffect::InitObject(AActor* Actor, FName NameBoneHit)
{
	myActor = Actor;
	NameBone = NameBoneHit;

	ITDSIGameActor* myInterface = Cast<ITDSIGameActor>(myActor);
	if (myInterface)
	{
		myInterface->Execute_AddEffect(myActor, this);
	}

	return true;
}

void UTDSStateEffect::DestroyObject()
{
	ITDSIGameActor* myInterface = Cast<ITDSIGameActor>(myActor);
	if (myInterface)
	{
		myInterface->Execute_RemoveEffect(myActor, this);
	}

	myActor = nullptr;
	if (this && this->IsValidLowLevel())
	{
		this->ConditionalBeginDestroy();
	}
}

bool UTDSStateEffect_ExecuteOnce::InitObject(AActor* Actor, FName NameBoneHit)
{
	Super::InitObject(Actor, NameBoneHit);
	ExecuteOnce();
	return true;
}

void UTDSStateEffect_ExecuteOnce::DestroyObject()
{
	Super::DestroyObject();
}

void UTDSStateEffect_ExecuteOnce::ExecuteOnce()
{
	if (myActor)
	{
		UTDSHealthComponent* myHealthComp = Cast<UTDSHealthComponent>(myActor->GetComponentByClass(UTDSHealthComponent::StaticClass()));
		if (myHealthComp)
		{
			myHealthComp->ChangeHealthValue_OnServer(Power);
		}
	}

	DestroyObject();
}

bool UTDSStateEffect_ExecuteTimer::InitObject(AActor* Actor, FName NameBoneHit)
{
	Super::InitObject(Actor, NameBoneHit);

	if (GetWorld()) 
	{
		GetWorld()->GetTimerManager().SetTimer(TimerHandle_EffectTimer, this,
			&UTDSStateEffect_ExecuteTimer::DestroyObject, Timer, false);
		GetWorld()->GetTimerManager().SetTimer(TimerHandle_ExecuteTimer, this,
			&UTDSStateEffect_ExecuteTimer::Execute, RateTime, true);
	}

	if (ParticleEffect)
	{
		/*FName NameBoneToAttached;
		FVector Loc = FVector(0);

		USceneComponent* myMesh = Cast<USceneComponent>(myActor->GetComponentByClass(USkeletalMeshComponent::StaticClass()));
		if (myMesh)
		{
			ParticleEmitter = UGameplayStatics::SpawnEmitterAttached(ParticleEffect,
				myMesh, NameBoneToAttached, Loc,
				FRotator::ZeroRotator, EAttachLocation::SnapToTarget, false);
		}
		else
		{
			ParticleEmitter = UGameplayStatics::SpawnEmitterAttached(ParticleEffect,
				myActor->GetRootComponent(), NameBoneToAttached, Loc,
				FRotator::ZeroRotator, EAttachLocation::SnapToTarget, false);
		}*/
	}

	return true;
}

void UTDSStateEffect_ExecuteTimer::DestroyObject()
{
	if (GetWorld())
	{
		GetWorld()->GetTimerManager().ClearAllTimersForObject(this);
	}

	/*if (ParticleEmitter)
	{
		ParticleEmitter->DestroyComponent();
		ParticleEmitter = nullptr;
	}*/
	Super::DestroyObject();
}

void UTDSStateEffect_ExecuteTimer::Execute()
{
	if (myActor)
	{
		/*UGameplayStatics::ApplyDamage(myActor, Power, nullptr,
			nullptr, nullptr);*/
		UTDSHealthComponent* myHealthComp = Cast<UTDSHealthComponent>(myActor->GetComponentByClass(UTDSHealthComponent::StaticClass()));
		if (myHealthComp)
		{
			myHealthComp->ChangeHealthValue_OnServer(Power);
		}	
	}
}

bool UTDSStateEffect_Stun::InitObject(AActor* Actor, FName NameBoneHit)
{
	Super::InitObject(Actor, NameBoneHit);
	Execute();

	GetWorld()->GetTimerManager().SetTimer(TimerHandle_StunTimer, this,
		&UTDSStateEffect_Stun::DestroyObject, Timer, false);

	return true;
}

void UTDSStateEffect_Stun::DestroyObject()
{
	if (myActor)
	{
		ATDSCharacter* Character = Cast<ATDSCharacter>(myActor);
		if (Character)
		{
			if (CharHealthComponent)
			{
				CharHealthComponent->SetIsAlive(true);
			}

			//Character->SetIsAlive(true);
			APlayerController* PCActor = Cast<APlayerController>(Cast<APawn>(myActor)->GetController());
			if (PCActor)
			{
				Character->EnableInput(PCActor);
			}
		}
	}

	Super::DestroyObject();
}

void UTDSStateEffect_Stun::Execute()
{
	if (myActor)
	{
		ATDSCharacter* Character = Cast<ATDSCharacter>(myActor);
		if (Character)
		{
			if (CharHealthComponent)
			{
				CharHealthComponent->SetIsAlive(false);
			}
			
			//Character->SetIsAlive(false);
			APlayerController* PCActor = Cast<APlayerController>(Cast<APawn>(myActor)->GetController());
			if (PCActor)
			{
				Character->DisableInput(PCActor);
			}
		}		
	}
}

void UTDSStateEffect::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);
	DOREPLIFETIME(UTDSStateEffect, NameBone);
}
