// Fill out your copyright notice in the Description page of Project Settings.


#include "Structure/TDSWorldItemDefault.h"

// Sets default values
ATDSWorldItemDefault::ATDSWorldItemDefault()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void ATDSWorldItemDefault::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void ATDSWorldItemDefault::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

