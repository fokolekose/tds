// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Components/ArrowComponent.h"

#include "FuncLibrary/Types.h"
#include "Weapon/ProjectileDefault.h"

#include "WeaponDefault.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnWeaponFireStart, UAnimMontage*, AnimFireChar);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnWeaponReloadStart, UAnimMontage*, AnimReloadChar);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_TwoParams(FOnWeaponReloadEnd, bool, bIsSuccess, int32, AmmoSafe);

UCLASS()
class TDS_API AWeaponDefault : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AWeaponDefault();

	FOnWeaponFireStart OnWeaponFireStart;
	FOnWeaponReloadStart OnWeaponReloadStart;
	FOnWeaponReloadEnd OnWeaponReloadEnd;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"), Category = Components)
	class USceneComponent* SceneComponent = nullptr;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"), Category = Components)
	class USkeletalMeshComponent* SkeletalMeshWeapon = nullptr;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"), Category = Components)
	class UStaticMeshComponent* StaticMeshWeapon = nullptr;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"), Category = Components)
	class UArrowComponent* ShootLocation = nullptr;

	UPROPERTY(VisibleAnywhere)
	FWeaponInfo WeaponSetting;
	UPROPERTY(Replicated, EditAnywhere, BlueprintReadWrite, Category = "Weapon Info")
	FAdditionalWeaponInfo AdditionalWeaponInfo;

	//Timers
	float FireTimer = 0.0f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "ReloadLogic")
		float ReloadTimer = 0.0f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "ReloadLogic Debug") //Debug!!!
		float ReloadTime = 0.0f;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Fire Logic")
		FName IdWeaponName;

	//Flags
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "FireLogic")
		bool WeaponFiring = false;
	UPROPERTY(Replicated, EditAnywhere, BlueprintReadWrite, Category = "ReloadLogic")
		bool WeaponReloading = false;
	bool WeaponAiming = false;
	bool BlockFire = false;

	//Dispersion
	UPROPERTY(Replicated)
	bool ShouldReduceDispersion = false;

	float CurrentDispersion = 0.0f;
	float CurrentDispersionMax = 1.0f;
	float CurrentDispersionMin = 0.1f;
	float CurrentDispersionRecoil = 0.1f;
	float CurrentDispersionReduction = 0.1f;

	//Time drop magazine on reload
	bool DropClipFlag = false;
	float DropClipTimer = -1.0f;

	//Shell flag
	bool DropShellFlag = false;
	float DropShellTimer = -1.0f;

	UPROPERTY(Replicated)
	FVector ShootEndLocation = FVector(0);

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	//Tick func end
	void FireTick(float DeltaTime);
	void ReloadTick(float DeltaTime);
	void DispersionTick(float DeltaTime);
	void ClipDropTick(float DeltaTime);
	void ShellDropTick(float DeltaTime);
	void WeaponInit();

	UFUNCTION()
		void Fire();

public:	
	// Tick func
	virtual void Tick(float DeltaTime) override;

	UFUNCTION(Server, Reliable, BlueprintCallable)
	void SetWeaponStateFire_OnServer(bool bIsFire);

	bool CheckWeaponCanFire();

	FProjectileInfo GetProjectile();	

	UFUNCTION(Server, Reliable)
	void UpdateStateWeapon_OnServer(EMovementState NewMovementState);
	void ChangeDispersionByShot();
	float GetCurrentDispersion() const;
	FVector ApplyDispersionToShoot(FVector DirectionShoot) const;

	FVector GetFireEndLocation() const;
	int8 GetNumberProjectileByShot() const;	

	UFUNCTION(BlueprintCallable)
	int32 GetWeaponRound();

	UFUNCTION()
	void InitReload();
	void FinishReload();
	void CancelReload();

	bool CheckCanWeaponReload();
	int8 GetAvailableAmmoForReload();

	UFUNCTION(Server, Reliable)
	void InitDropMesh_OnServer(UStaticMesh* DropMesh, FTransform Offset, FVector DropImpulseDirection,
		float LifeTimeMesh, float ImpulseRandomDispersion, float PowerImpulse, float CustomMass);

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Debug")
	bool ShowDebug = false;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Debug")
	float SizeVectorToChangeShootDirectionLogic = 100.0f;

	//
	UFUNCTION(Server, Unreliable)
	void UpdateWeaponByCharacterMovementState_OnServer(FVector NewShootEndLocation, bool NewShouldReduceDispersion);

	UFUNCTION(NetMulticast, Unreliable)
	void AnimWeaponStart_Multicast(UAnimMontage* Anim);
	UFUNCTION(NetMulticast, Unreliable)
	void ShellDropFire_Multicast(UStaticMesh* DropMesh, FTransform Offset, FVector DropImpulseDirection,
		float LifeTimeMesh, float ImpulseRandomDispersion, float PowerImpulse, float CustomMass, FVector LocalDir);

	UFUNCTION(NetMulticast, Unreliable)
	void FXWeaponFire_Multicast(UParticleSystem* FXFire, USoundBase* SoundFire);

	UFUNCTION(NetMulticast, Unreliable)
	void WeaponTraceFireSpawnDecalAttached_Multicast(UMaterialInterface* MyMaterial, FVector DecalSize, USceneComponent* AttachToComponent,
		FName AttachPointName, FVector Location, FRotator Rotation, EAttachLocation::Type TypeAttach, float LifeSpan);
	UFUNCTION(NetMulticast, Unreliable)
	void SpawnTraceFireEmitterAtLocation_Multicast(UWorld* World, UParticleSystem* EmitterTemplate, const FTransform& SpawnTransform);
	UFUNCTION(NetMulticast, Unreliable)
	void PlaySoundTraceFireAtLocation_Multicast(UWorld* World, USoundBase* Sound, FVector Location);
	UFUNCTION(NetMulticast, Unreliable)
	void AddEffectTraceFireBySurfaceType_Multicast(AActor* TakeEffectActor, FName NameBoneHit, TSubclassOf<UTDSStateEffect> EffectClass, EPhysicalSurface SurfaceType);
	UFUNCTION(NetMulticast, Reliable)
	void ApplyPointDamageTraceFire_Multicast(AActor* DamagedActor, float Damage, const FVector& HitFromDirection,
		const FHitResult& HitInfo, AController* EventInstigator);

	UFUNCTION(NetMulticast, Reliable)
	void WeaponFireStartEvent_Multicast(UAnimMontage* AnimFireChar);
	UFUNCTION(NetMulticast, Reliable)
	void WeaponReloadStartEvent_Multicast(UAnimMontage* AnimReloadChar);
	UFUNCTION(NetMulticast, Reliable)
	void WeaponReloadEndEvent_Multicast(bool bIsSuccess, int32 AmmoSafe);
};
