// Fill out your copyright notice in the Description page of Project Settings.


#include "Weapon/ProjectileDefault_Grenade.h"
#include "Kismet/GameplayStatics.h"
#include "Interface/TDSIGameActor.h"
#include "DrawDebugHelpers.h"

int32 DebugExplodeShow = 0;
FAutoConsoleVariableRef CVARExplodeShow(
	TEXT("TDS.DebugExplode"),
	DebugExplodeShow,
	TEXT("Draw Debug for Explode"),
	ECVF_Cheat);

void AProjectileDefault_Grenade::BeginPlay()
{
	Super::BeginPlay();
}

void AProjectileDefault_Grenade::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	if (HasAuthority())
	{
		TimerExplose(DeltaTime);
	}	
}

void AProjectileDefault_Grenade::TimerExplose(float DeltaTime)
{
	if (TimerEnabled)
	{
		if (TimerToExplose > TimeToExplose)
		{
			//Explose
			Explose_OnServer();
		}
		else
		{
			TimerToExplose += DeltaTime;
		}
	}
}

void AProjectileDefault_Grenade::BulletCollisionSphereHit(class UPrimitiveComponent* HitComp, AActor* OtherActor,
	UPrimitiveComponent* OtherComp, FVector NormalImpulse, const FHitResult& Hit)
{
	if (!TimerEnabled)
	{
		Explose_OnServer();
	}
	Super::BulletCollisionSphereHit(HitComp, OtherActor, OtherComp, NormalImpulse, Hit);
}

void AProjectileDefault_Grenade::ImpactProjectile()
{
	//Init Grenade
	TimerEnabled = true;
}

void AProjectileDefault_Grenade::Explose_OnServer_Implementation()
{
	if (DebugExplodeShow)
	{
		DrawDebugSphere(GetWorld(), GetActorLocation(), ProjectileSetting.ProjectileMaxRadiusDamage, 12, FColor::Green, false, 12.0f);
		DrawDebugSphere(GetWorld(), GetActorLocation(), ProjectileSetting.ProjectileMinRadiusDamage, 12, FColor::Red, false, 12.0f);
	}

	TimerEnabled = false;
	if (ProjectileSetting.ExplodeFX)
	{
		SpawnEmitterAtLocation_Multicast(GetWorld(), ProjectileSetting.ExplodeFX, GetActorLocation(), GetActorRotation(), FVector(1.0f));
		/*UGameplayStatics::SpawnEmitterAtLocation(GetWorld(), ProjectileSetting.ExplodeFX,
			GetActorLocation(), GetActorRotation(), FVector(1.0f));*/
	}
	if (ProjectileSetting.ExplodeSound)
	{
		PlaySoundAtLocation_Multicast(GetWorld(), ProjectileSetting.ExplodeSound, GetActorLocation());
		//UGameplayStatics::PlaySoundAtLocation(GetWorld(), ProjectileSetting.ExplodeSound, GetActorLocation());
	}
	
	TArray<AActor*> IgnoredActor;
	ApplyRadialDamageWithFalloff_Multicast(GetWorld(), ProjectileSetting.ExplodeMaxDamage, ProjectileSetting.ExplodeMaxDamage * 0.2f,
		GetActorLocation(), ProjectileSetting.ProjectileMinRadiusDamage, ProjectileSetting.ProjectileMaxRadiusDamage, 5);
	/*UGameplayStatics::ApplyRadialDamageWithFalloff(GetWorld(), ProjectileSetting.ExplodeMaxDamage,
		ProjectileSetting.ExplodeMaxDamage * 0.2f, GetActorLocation(),
		ProjectileSetting.ProjectileMinRadiusDamage, ProjectileSetting.ProjectileMaxRadiusDamage,
		5, NULL, IgnoredActor, this, nullptr);*/

	this->Destroy();
}

void AProjectileDefault_Grenade::SpawnEmitterAtLocation_Multicast_Implementation(const UObject* WorldContextObject, UParticleSystem* EmitterTemplate, FVector Location, FRotator Rotation, FVector Scale)
{
	UGameplayStatics::SpawnEmitterAtLocation(WorldContextObject, EmitterTemplate, Location, Rotation, Scale);
}

void AProjectileDefault_Grenade::PlaySoundAtLocation_Multicast_Implementation(const UObject* WorldContextObject, USoundBase* Sound, FVector Location)
{
	UGameplayStatics::PlaySoundAtLocation(WorldContextObject, Sound, Location);
}

void AProjectileDefault_Grenade::ApplyRadialDamageWithFalloff_Multicast_Implementation(const UObject* WorldContextObject, float BaseDamage, float MinimumDamage, const FVector& Origin, float DamageInnerRadius, float DamageOuterRadius, float DamageFalloff)
{
	TArray<AActor*> IgnoredActors;
	UGameplayStatics::ApplyRadialDamageWithFalloff(WorldContextObject, BaseDamage, MinimumDamage, Origin,
		DamageInnerRadius, DamageOuterRadius, DamageFalloff, NULL, IgnoredActors, this, nullptr);
}
