// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "FuncLibrary/Types.h"
#include "TDSInventoryComponent.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE_ThreeParams(FOnSwitchWeapon, FName, WeaponIdName, FAdditionalWeaponInfo, WeaponAdditionalInfo, int32, NewCurrentIndexWeapon);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_TwoParams(FOnAmmoChange, EWeaponType, TypeAmmo, int32, Cout);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_TwoParams(FOnWeaponAdditionalInfoChange, int32, IndexSlot, FAdditionalWeaponInfo, AdditionalInfo);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnWeaponAmmoEmpty, EWeaponType, WeaponType);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnWeaponAmmoAvailable, EWeaponType, WeaponType);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_TwoParams(FOnUpdateWeaponSlots, int32, IndexSlotChange, FWeaponSlot, NewInfo);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnWeaponNotHaveRound, int32, IndexSlotWeapon);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnWeaponHaveRound, int32, IndexSlotWeapon);

UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class TDS_API UTDSInventoryComponent : public UActorComponent
{
	GENERATED_BODY()

public:	
	// Sets default values for this component's properties
	UTDSInventoryComponent();

	UPROPERTY(BlueprintAssignable, Category = "Inventory")
		FOnSwitchWeapon OnSwitchWeapon;
	//Event on change ammo in slots by WeaponType
	UPROPERTY(BlueprintAssignable, Category = "Inventory")
		FOnAmmoChange OnAmmoChange;
	UPROPERTY(BlueprintAssignable, Category = "Inventory")
		FOnWeaponAdditionalInfoChange OnWeaponAdditionalInfoChange;
	//Event ammo slots after change still empty rounds
	UPROPERTY(BlueprintAssignable, Category = "Inventory")
		FOnWeaponAmmoEmpty OnWeaponAmmoEmpty;
	//Event ammo slots after change have round
	UPROPERTY(BlueprintAssignable, Category = "Inventory")
		FOnWeaponAmmoAvailable OnWeaponAmmoAvailable;
	//Event weapon was change by SlotIndex
	UPROPERTY(BlueprintAssignable, Category = "Inventory")
		FOnUpdateWeaponSlots OnUpdateWeaponSlots;
	//Event current weapon not have additional rounds
	UPROPERTY(BlueprintAssignable, Category = "Inventory")
		FOnWeaponNotHaveRound OnWeaponNotHaveRound;
	//Event current weapon have additional rounds
	UPROPERTY(BlueprintAssignable, Category = "Inventory")
		FOnWeaponHaveRound OnWeaponHaveRound;

	UPROPERTY(Replicated, EditAnywhere, BlueprintReadOnly, Category = "Weapons")
		TArray<FWeaponSlot> WeaponSlots;
	UPROPERTY(Replicated, EditAnywhere, BlueprintReadOnly, Category = "Weapons")
		TArray<FAmmoSlot> AmmoSlots;

	int32 MaxSlotsWeapon = 0;

protected:
	// Called when the game starts
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;
	
	bool SwitchWeaponToIndexByNextPreviousIndex(int32 ChangeToIndex, int32 OldIndex, FAdditionalWeaponInfo OldInfo, bool bIsForward);
	bool SwitchWeaponByIndex(int32 IndexWeaponToChange, int32 PreviousIndex, FAdditionalWeaponInfo PreviousWeaponInfo);

	void SetAdditionalInfoWeapon(int32 IndexWeapon, FAdditionalWeaponInfo NewInfo);

	FAdditionalWeaponInfo GetAdditionalInfoWeapon(int32 IndexWeapon);
	int32 GetWeaponIndexSlotByName(FName IdWeaponName);
	FName GetWeaponNameBySlotIndex(int32 IndexSlot);
	bool GetWeaponTypeByIndexSlot(int32 IndexSlot, EWeaponType& WeaponType);
	bool GetWeaponTypeByNameWeapon(FName IdWeaponName, EWeaponType& WeaponType);
	

	UFUNCTION(BlueprintCallable)
		void AmmoSlotChangeValue(EWeaponType TypeWeapon, int32 CoutChangeAmmo);
	bool CheckAmmoForWeapon(EWeaponType TypeWeapon, int8& AvailableAmmoForWeapon);

	//Interface PickUp Actors
	UFUNCTION(BlueprintCallable, Category = "Interface")
		bool CheckCanTakeAmmo(EWeaponType AmmoType);
	UFUNCTION(BlueprintCallable, Category = "Interface")
		bool CheckCanTakeWeapon(int32& FreeSlot);
	UFUNCTION(BlueprintCallable, Category = "Interface")
		bool SwitchWeaponToInventory(FWeaponSlot NewWeapon, int32 IndexSlot, int32 CurrentIndexWeaponChar, FDropItem& DropItemInfo);
	UFUNCTION(Server, Reliable, BlueprintCallable, Category = "Interface")
		void TryGetWeaponToInventory_OnServer(AActor* PickUpActor, FWeaponSlot NewWeapon);
	UFUNCTION(Server, Reliable, BlueprintCallable, Category = "Interface")
		void DropWeaponByIndex_OnServer(int32 ByIndex);
	UFUNCTION(BlueprintCallable, Category = "Interface")
		bool GetDropItemInfoFromInventory(int32 IndexSlot, FDropItem& DropItemInfo);

	UFUNCTION(BlueprintCallable, Category = "Inventory")
		TArray<FWeaponSlot> GetWeaponSlots();
	UFUNCTION(BlueprintCallable, Category = "Inventory")
		TArray<FAmmoSlot> GetAmmoSlots();

	UFUNCTION(Server, Reliable, BlueprintCallable, Category = "Inventory")
		void InitInventory_OnServer(const TArray<FWeaponSlot>& NewWeaponSlotsInfo, const TArray<FAmmoSlot>& NewAmmoSlotsInfo);

	UFUNCTION(Server, Reliable, BlueprintCallable, Category = "Inventory")
		void SwitchWeaponEvent_OnServer(FName WeaponIdName, FAdditionalWeaponInfo WeaponAdditionalInfo, int32 NewCurrentIndexWeapon);
	UFUNCTION(NetMulticast, Reliable, BlueprintCallable, Category = "Inventory")
		void AmmoChangeEvent_Multicast(EWeaponType TypeAmmo, int32 Cout);
	UFUNCTION(NetMulticast, Reliable, BlueprintCallable, Category = "Inventory")
		void WeaponAdditionalInfoChangeEvent_Multicast(int32 IndexSlot, FAdditionalWeaponInfo AdditionalInfo);
	UFUNCTION(NetMulticast, Reliable, BlueprintCallable, Category = "Inventory")
		void WeaponAmmoEmptyEvent_Multicast(EWeaponType WeaponType);
	UFUNCTION(NetMulticast, Reliable, BlueprintCallable, Category = "Inventory")
		void WeaponAmmoAvailableEvent_Multicast(EWeaponType WeaponType);
	UFUNCTION(NetMulticast, Reliable, BlueprintCallable, Category = "Inventory")
		void UpdateWeaponSlotsEvent_Multicast(int32 IndexSlotChange, FWeaponSlot NewInfo);
	UFUNCTION(NetMulticast, Reliable, BlueprintCallable, Category = "Inventory")
		void WeaponNotHaveRoundEvent_Multicast(int32 IndexSlotWeapon);
	UFUNCTION(NetMulticast, Reliable, BlueprintCallable, Category = "Inventory")
		void WeaponHaveRoundEvent_Multicast(int32 IndexSlotWeapon);
};
